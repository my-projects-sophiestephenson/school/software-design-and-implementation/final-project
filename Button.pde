/*  
 *  Button Class
 *
 *  Tieren Costello, Ian Rosevear, Sophie Stephenson
 *
 *  Creates a button with either a label or icon that can be clicked.
 */

class Button {
  private float x, y, w, h;
  private String label;
  private PImage icon;
  private color textColor, buttonColor, outlineColor;

  public Button(float x, float y, float w, float h, String label, color textColor, color buttonColor, color outlineColor) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.icon = null;
    this.label = label;
    this.textColor = textColor;
    this.buttonColor = buttonColor;
    this.outlineColor = outlineColor;
  }
  
  public Button(float x, float y, float w, float h, String label, color textColor, color buttonColor) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.icon = null;
    this.label = label;
    this.textColor = textColor;
    this.buttonColor = buttonColor;
    this.outlineColor = buttonColor;
  }

  public Button(float x, float y, float w, float h, PImage icon, color buttonColor, color outlineColor) {
    this(x, y, w, h, "", 0, buttonColor, outlineColor);
    this.icon = icon;
  }

  void draw() {
    // Draw the button shape
    fill(buttonColor);
    stroke(outlineColor);
    strokeWeight(1);
    rect(x, y, w, h, 5);

    // Either place icon inside or write the text label
    if (icon != null) {
      image(icon, x, y, w/2, h/2);
    } else {
      fill(textColor);
      textFont(REGULAR);
      textSize(50);
      text(label, x, y + 15);
    }
  }

  // Return whether the button was clicked
  boolean wasClicked() {
    return mouseX >= x - w/2 && mouseX <= x + w/2 && mouseY >= y - h/2 && mouseY <= y + h/2;
  }
}
