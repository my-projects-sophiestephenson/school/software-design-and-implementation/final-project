/*  
 *  Event Class
 *
 *  Tieren Costello, Ian Rosevear, Sophie Stephenson
 *
 *  An event, holding a name, date/time, and location, for use in
 *  storing the calendar. 
 */
 
import java.util.*;

class Event {
  // Fields
  private String name;
  private GregorianCalendar time;
  private String location;

  // Constructor
  Event(String name, GregorianCalendar time, String location) {
    this.name = name;
    this.time = time;
    this.location = location;
  }

  // Getters & Setters
  public String getName() { 
    return name;
  }
  public GregorianCalendar getTime() { 
    return time;
  }
  public int getYear() {
    return time.get(Calendar.YEAR);  
  }
  public int getMonth() {
    return time.get(Calendar.MONTH);
  }
  public int getDayOfMonth() {
    return time.get(Calendar.DAY_OF_MONTH);  
  }
  public int getHour() {
    return time.get(Calendar.HOUR);  
  }
  public int getMinute() {
    return time.get(Calendar.MINUTE);  
  }
  public String getLocation() { 
    return location;
  }
  public String getTimeString() {
    String ampm = time.get(Calendar.AM_PM) == Calendar.AM ? "am" : "pm";
    String addZero = getMinute() < 10 ? "0" : "";
    return getHour() + ":" + addZero + getMinute() + ampm;
  }

  public void setName(String newName) {
    name = newName;
  }
  public void setTime(GregorianCalendar newTime) {
    time = newTime;
  }
  public void setLocation(String newLocation) {
    location = newLocation;
  }
  
  // Returns whether the event has passed
  public boolean hasPassed() {
    return time.before(Calendar.getInstance());  
  }
  
  // Returns the hours and minutes until the event, in the format [hh, mm]
  public int[] timeUntil() {
    // Get current hours/minutes and event time hours/minutes
    int evHour = getHour();
    int evMin = getMinute();
    int currHour = hour();   
    int currMin = minute();     

    // Get hours and minutes left until event and return 
    int hoursLeft = evHour - currHour;
    if (evHour < currHour)
      hoursLeft += 12;
      
    int minutesLeft = evMin - currMin;
    if (evMin < currMin) {
      minutesLeft += 60;
      hoursLeft--;    // Account for the hour carried to the minutes
    }
    
    return new int[] { hoursLeft, minutesLeft };
  }

  // Returns a string explaining the time until the next event
  public String displayTimeUntil() {
    // Get hours and minutes left
    int[] timeUntil = this.timeUntil();
    int hoursLeft = timeUntil[0];
    int minutesLeft = timeUntil[1];

    // Format string
    if (hoursLeft > 0) {
      String hourPlural = hoursLeft > 1 ? "s" : "";
      if (minutesLeft > 0) 
        return hoursLeft + " hour" + hourPlural + " " + minutesLeft + " min.";
      else 
        return hoursLeft + " hour" + hourPlural;
    } else {
      String minPlural = minutesLeft > 1 ? "s" : "";
      return minutesLeft + " minute" + minPlural;
    }
  }
}
