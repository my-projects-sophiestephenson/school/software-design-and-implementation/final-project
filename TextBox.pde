/*  
 *  TextBox Class
 *
 *  Tieren Costello, Ian Rosevear, Sophie Stephenson
 *
 *  A text box which can be put into focus and can be typed into if
 *  in focus.
 */
 
 class TextBox {
  // Attributes
  private float x, y;
  private float w, h;
  private boolean inFocus;
  private String label;
  private String typing, typingBuffer;

  // Constructor
  public TextBox(String label, float x, float y, float w) {
    this.label = label;
    this.x = x;
    this.y = y;
    this.w = w;
    h = 75;
    inFocus = false;
    typing = "";
    typingBuffer = "";
  }

  // Methods
  public float getX() { 
    return x;
  }
  public float getY() { 
    return y;
  }
  public float getW() { 
    return w;
  }
  public float getH() { 
    return h;
  }
  public boolean isInFocus() { 
    return inFocus;
  }
  public String getTyping() { 
    return typing;
  }
  public boolean isBufferUpdated() { 
    return typing == typingBuffer;
  }

  public void makeInFocus() { 
    inFocus = true;
  }
  public void makeOutOfFocus() { 
    inFocus = false;
  }
  public void updateTyping(char c) { 
    typing += c;
  }
  public void clearTyping() { 
    typing = "";
  }
  public void delete() { 
    int len = typing.length();
    if (len > 0) typing = typing.substring(0, len - 1);
  }
  public void updateBuffer() { 
    typingBuffer = typing;
  }
  public boolean wasClicked() {
    return mouseX >= x - w/2 && mouseX <= x + w/2 && mouseY >= y - h/2 && mouseY <= y + h/2;
  }

  void draw() {
    textAlign(CORNER);
    
    // Draw the label
    textFont(REGULAR);
    textSize(35);
    fill(150, 0, 0);
    text(label, x, y - 10);
    
    // Draw box (with a larger red outline if in focus)
    if (inFocus) {
      stroke(150, 0, 0);
      strokeWeight(2);
    } else {
      stroke(150);
      strokeWeight(1);
    }
    fill(255);
    rect(x + w/2, y + h/2, w, h);
    
    // Write the text that has been typed into the box
    fill(0);
    textSize(30);
    text(typing, x + 13, y + 48);
    textAlign(CENTER);
  }
}
