/*  
 *  CMPU 203 Final Project
 *
 *  Tieren Costello, Ian Rosevear, Sophie Stephenson
 *
 *  Displays either information about the next event in the client’s 
 *  schedule or information about the current date and time. Events can 
 *  be hard-coded or entered manually through a menu accessible from the 
 *  home screen of the device.
 * 
 */

// When switching from Java to Android mode:
//     Uncomment the android import line, the open/closeKeyboard(), and the KEYCODE_DEL
//     Also comment out the size() instruction and uncomment the fullscreen()
//     Finally, uncomment the text to speech stuff (only available on android)

import java.util.*;
import java.time.*;
/* 
import android.view.KeyEvent;        
import android.media.MediaPlayer;
import android.content.res.Resources;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.content.Context;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
*/

// Constants
final Calendar NOW = Calendar.getInstance();      // Current date/time calendar
final TextBox NAME_BOX = new TextBox("Event name:", 50, 120, 550);             // Text boxes to hold input
final TextBox DATE_BOX = new TextBox("Date:", 650, 120, 250);  
final TextBox TIME_BOX = new TextBox("Time:", 50, 255, 300);
final TextBox LOCATION_BOX = new TextBox("Location:", 400, 255, 500);
final TextBox[] INPUT_BOXES = { NAME_BOX, DATE_BOX, TIME_BOX, LOCATION_BOX };  // Container for text boxes
LinkedList<Event> CLIENT_CAL = new LinkedList<Event>();  // Our calendar of events
/*
final MediaPlayer NO_EVENT_SND = new MediaPlayer();    // Media players for each sound
final MediaPlayer SHARON_SND = new MediaPlayer();
final MediaPlayer THREE_SND = new MediaPlayer();
final MediaPlayer THREE_15_SND = new MediaPlayer();
final MediaPlayer THREE_30_SND = new MediaPlayer();
final MediaPlayer THREE_45_SND = new MediaPlayer();
final MediaPlayer THREE_49_SND = new MediaPlayer();
final MediaPlayer FOUR_SND = new MediaPlayer();
final MediaPlayer FOUR_15_SND = new MediaPlayer();
final String SHARON_VARIETY = "sharon.m4a";            // Strings for each sound file
final String NO_EVENT = "no-events.m4a";
final String AROUND_3 = "300.m4a";
final String AROUND_315 = "315.m4a";
final String AROUND_330 = "330.m4a";
final String AROUND_345 = "345.m4a";
final String PRECISELY_349 = "349.m4a";
final String AROUND_4 = "400.m4a";
final String AROUND_415 = "415.m4a";
static final String TAG = "AudioSoundLib";
*/

// Constants initialized on setup           
Button DARK_AUDIO_BUTTON, LIGHT_AUDIO_BUTTON, ADD_EVENT_BUTTON;  // Buttons for TTS and adding events
PImage LIGHT_PLUS_ICON, DARK_PLUS_ICON, EXIT_ICON;               // Icons for switching screens
PImage WHITE_AUDIO_ICON, GRAY_AUDIO_ICON;                        // Icons for audio buttons
PFont BOLD, ITALIC, REGULAR;                                     // Fonts
/*
Activity ACTIVITY;             // TTS Stuff 
Context CONTEXT;
AssetFileDescriptor FD;
*/

// Variables
boolean displayNextEvent = false;         // What should be displayed
boolean displaySettings = false;          
boolean displaySettingsBuffer = false;    // Buffers for the above
boolean displayEventBuffer = false;  
boolean keyboardOpen = false;                  
boolean displayAddedMessage = false;     // Should the "Event added!" message be displayed
boolean displayTimeWarning = false;      // Should the warning messages be displayed
boolean displayDateWarning = false;  
TextBox inFocusBox = null;           // Which box is in focus, and buffer
TextBox inFocusBuffer = null;   
int minuteBuffer = -1;               // Holds the last recorded value of the current minute
Event nextEvent;                     // The next event on the calendar
boolean soundLoaded = false;         // TTS stuff
int resumePosition;  

void setup() {
  orientation(LANDSCAPE);
  //fullScreen();        // For use on the tablet
  size(1024, 600);   // For debugging with Processing
  textAlign(CENTER);
  rectMode(CENTER);
  imageMode(CENTER);
  background(245);
  smooth();

  // Load fonts
  BOLD = loadFont("Verdana-Bold-150.vlw");
  REGULAR = loadFont("Verdana-150.vlw");
  ITALIC = loadFont("Verdana-Italic-150.vlw");

  // Load icons
  DARK_PLUS_ICON = loadImage("dark-plus.png");
  LIGHT_PLUS_ICON = loadImage("light-plus.png");
  EXIT_ICON = loadImage("exit.png");
  WHITE_AUDIO_ICON = loadImage("white-audio.png");
  GRAY_AUDIO_ICON = loadImage("gray-audio.png");

  // Create buttons
  DARK_AUDIO_BUTTON = new Button(895, 290, 150, 150, WHITE_AUDIO_ICON, 150, 100);
  LIGHT_AUDIO_BUTTON = new Button(895, 290, 150, 150, GRAY_AUDIO_ICON, 255, 150);
  ADD_EVENT_BUTTON = new Button(width/2, 3 * height/4, width/2, 100, "Add event", 255, color(150, 0, 0));

  // Make calendar and get next event
  makeCalendar();    
  nextEvent = CLIENT_CAL.removeFirst();    

  // Set up TTS
  setUpTTS();
}

void draw() {
  // Loop through the calendar until we get to the first event that hasn't passed, or the last event in the calendar
  while (nextEvent.hasPassed() && CLIENT_CAL.peek() != null) {
    nextEvent = CLIENT_CAL.removeFirst();
  }

  // Only update the display if the minute has changed or the display has changed in any way
  if (minuteBuffer != minute() || displayEventBuffer != displayNextEvent
    || displaySettingsBuffer != displaySettings || inFocusBuffer != inFocusBox
    || (inFocusBox != null && !inFocusBox.isBufferUpdated())) {

    // Update currMinute and buffers
    minuteBuffer = minute();
    displayNextEvent = displayEventBuffer;
    displaySettings = displaySettingsBuffer;
    inFocusBox = inFocusBuffer;

    // Display settings if necessary
    if (displaySettings) {
      displayAddEvent();

      // Else, display the calendar stuff
    } else {

      // Display the next event and the following event
      if (displayNextEvent) {
        displayEventInformation();
        DARK_AUDIO_BUTTON.draw();
        image(DARK_PLUS_ICON, width - 35, 35, 60, 60);
        
      
      // Display the current date and time  
      } else {
        displayCurrentDateTime();
        LIGHT_AUDIO_BUTTON.draw();
        image(LIGHT_PLUS_ICON, width - 35, 35, 60, 60);
      }
    }
  }
}

void mousePressed() {

  // Close the keyboard if it was open
  if (keyboardOpen) {
    //closeKeyboard();    // only available in android mode
    keyboardOpen = false;

    // If pressed on the settings/exit button, switch to settings screen or exit settings
  } else if (mouseX >= width - 70 && mouseX <= width - 18 && mouseY >= 17 && mouseY <= 70) {
    displaySettingsBuffer = !displaySettingsBuffer;

    // If on the settings screen...
  } else if (displaySettingsBuffer) {

    // If tapping on a box, it should come into focus
    for (TextBox box : INPUT_BOXES) {
      float x = box.getX();
      float y = box.getY();
      if (mouseX >= x && mouseX <= x + box.getW() && mouseY >= y && mouseY <= y + box.getH()) {
        setFocusBox(box);
        //openKeyboard();    // only available in android mode
        keyboardOpen = true;
        redraw();
        break;
      }
    }

    // Add event if the button was pressed
    if (ADD_EVENT_BUTTON.wasClicked()) {
      addNewEvent();
    }

    // If not on settings screen...
  } else {
    // Read audio if audio button pressed
    if (DARK_AUDIO_BUTTON.wasClicked()) {
      readDisplay();

      // Otherwise, switch the display
    } else {
      switchDisplay();
    }
  }
}

void keyPressed() {
  // If there's a box in focus at the moment...
  if (displaySettingsBuffer && inFocusBox != null) {

    // Delete a character from the in focus box if user pressed delete (include the KEYCODE.DEL
    //    statement if not in android mode)
    if (key == BACKSPACE || key == DELETE /* ||  keyCode == KeyEvent.KEYCODE_DEL */) inFocusBox.delete();

    // If pressing tab, it should tab to the next box
    else if (key == TAB && inFocusBox != LOCATION_BOX) {
      if (inFocusBox == NAME_BOX)      setFocusBox(DATE_BOX);
      else if (inFocusBox == DATE_BOX) setFocusBox(TIME_BOX);
      else if (inFocusBox == TIME_BOX) setFocusBox(LOCATION_BOX);

      // Otherwise, update the display to show what the user typed
    } else inFocusBox.updateTyping(key);
  }
}

void switchDisplay() {
  displayEventBuffer = !displayEventBuffer;
  redraw();  // Update screen accordingly
}

// ----------- Calendar / Event Methods -----------

// Set up the calendar of events
void makeCalendar() {
  CLIENT_CAL = new LinkedList<Event>();
  int DEC = 11;

//Monday the 9th
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 9, 9, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Food Committee", new GregorianCalendar(2019, DEC, 9, 10, 00), "Room RR"));
  CLIENT_CAL.add(new Event("Trivia", new GregorianCalendar(2019, DEC, 9, 13, 15), "Room 2N"));
  
  //Tuesday the 10th
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 10, 9, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Coffee Cart", new GregorianCalendar(2019, DEC, 10, 10, 30), "Room 2N"));
  CLIENT_CAL.add(new Event("Pamper & Polish", new GregorianCalendar(2019, DEC, 10, 13, 15), "Room 2N"));
  CLIENT_CAL.add(new Event("Word Game", new GregorianCalendar(2019, DEC, 10, 13, 30), "Room 2N"));
  CLIENT_CAL.add(new Event("Rich & Ricco Perform", new GregorianCalendar(2019, DEC, 10, 14, 30), "Room LR"));
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 10, 15, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Sharon's Variety Hour", new GregorianCalendar(2019, DEC, 10, 15, 45), "Room 2N"));
  
  //Wednesday the 11th
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 11, 9, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Protestant Service", new GregorianCalendar(2019, DEC, 1, 10, 30), "Room C"));
  CLIENT_CAL.add(new Event("Music Hour", new GregorianCalendar(2019, DEC, 11, 13, 15), "Room 2N"));
  CLIENT_CAL.add(new Event("Catholic Mass", new GregorianCalendar(2019, DEC, 11, 14, 0), "Room C"));
  CLIENT_CAL.add(new Event("Piano Lounge", new GregorianCalendar(2019, DEC, 11, 14, 15), "Room LR"));
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 1, 15, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Singchronisity Performs", new GregorianCalendar(2019, DEC, 1, 19, 0), "Room LR"));
  
  //Thursday the 12th
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 12, 9, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Bingo Sale", new GregorianCalendar(2019, DEC, 12, 10, 0), "Room RR"));
  CLIENT_CAL.add(new Event("Rhythm & Music", new GregorianCalendar(2019, DEC, 12, 13, 30), "Room 2S"));
  CLIENT_CAL.add(new Event("Elves Bingo", new GregorianCalendar(2019, DEC, 12, 14, 30), "Room LR"));
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 12, 15, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Sharon's Variety Hr.", new GregorianCalendar(2019, DEC, 12, 15, 45), "Room RR"));
  
  //Friday the 13th
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 13, 9, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Vincent Performs", new GregorianCalendar(2019, DEC, 13, 10, 30), "Room LR"));
  CLIENT_CAL.add(new Event("Women's Group", new GregorianCalendar(2019, DEC, 13, 13, 15), "Room 2S"));
  CLIENT_CAL.add(new Event("Word Puzzles", new GregorianCalendar(2019, DEC, 13, 13, 30), "Room 2N"));
  CLIENT_CAL.add(new Event("Hospice Choir", new GregorianCalendar(2019, DEC, 13, 14, 30), "Room LR"));
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 13, 15, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Sharon's Variety Hour", new GregorianCalendar(2019, DEC, 13, 15, 45), "Room RR"));
  
  //Saturday the 14th
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 14, 9, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Oakgrove Elementary School", new GregorianCalendar(2019, DEC, 14, 10, 0), "Room LR"));
  CLIENT_CAL.add(new Event("Exercise", new GregorianCalendar(2019, DEC, 14, 13, 15), "Room 1N"));
  CLIENT_CAL.add(new Event("Bingo", new GregorianCalendar(2019, DEC, 14, 14, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 14, 15, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Movie Time", new GregorianCalendar(2019, DEC, 14, 16, 0), "Room AU"));
  
  //Sunday the 15th
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 15, 9, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Bingo", new GregorianCalendar(2019, DEC, 15, 10, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Protestant Service", new GregorianCalendar(2019, DEC, 15, 14, 0), "Room C"));
  CLIENT_CAL.add(new Event("Piano Recital", new GregorianCalendar(2019, DEC, 15, 14, 45), "Room LR"));
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 15, 15, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Movie Time", new GregorianCalendar(2019, DEC, 15, 16, 0), "Room AU"));
  
  //Monday the 16th
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 16, 9, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Mixed Up Christmas Word Game", new GregorianCalendar(2019, DEC, 16, 10, 30), "Room 2S"));
  CLIENT_CAL.add(new Event("Bill & Wayne", new GregorianCalendar(2019, DEC, 16, 13, 15), "Room AU"));
  CLIENT_CAL.add(new Event("Choir", new GregorianCalendar(2019, DEC, 16, 13, 30), "Room C"));
  CLIENT_CAL.add(new Event("Birthday w/Bill & Wayne", new GregorianCalendar(2019, DEC, 16, 14, 30), "Room LR"));
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 16, 15, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Bingo", new GregorianCalendar(2019, DEC, 16, 19, 15), "Room LR"));
  
  //Tuesday the 17th
  CLIENT_CAL.add(new Event("Coffee Time", new GregorianCalendar(2019, DEC, 16, 9, 30), "Room RR"));
  CLIENT_CAL.add(new Event("Arlington Italian Club Choir", new GregorianCalendar(2019, DEC, 16, 10, 15), "Room LR"));
  CLIENT_CAL.add(new Event("Pamper & Polish", new GregorianCalendar(2019, DEC, 16, 13, 15), "Room 2N"));
  CLIENT_CAL.add(new Event("Music Hour", new GregorianCalendar(2019, DEC, 16, 13, 30), "Room 1N"));
  
}

// Add a new event of the user's input
void addNewEvent() {
  // No box should be in focus
  setFocusBox(null);

  // Get all necessary information from the boxes
  String name = NAME_BOX.getTyping();
  String location = LOCATION_BOX.getTyping();

  // Get date from the date box
  String date = DATE_BOX.getTyping();
  int month = 0, day = 0, year = 0;

  // If it's not in the right format, plan to display a warning
  if (match(date, "[1-9]?[0-9]/[1-9]?[0-9]/[0-9]{4}") == null) { 
    displayDateWarning = true;

    // Otherwise, get information from the string
  } else {
    String[] dateinfo = date.split("/");
    month = int(dateinfo[0]) - 1;
    day = int(dateinfo[1]);
    year = int(dateinfo[2]);
  }

  // Get time from the time box
  String time = TIME_BOX.getTyping();
  int hour = 0, minute = 0;

  // If it's not in the right format, plan to display a warning
  if (match(time, "1?[0-9].:[0-5][0-9][ap]m") == null) {
    displayTimeWarning = true;

    // Otherwise, get the information from the string
  } else {
    String[] timeinfo = time.split(":");
    hour = int(timeinfo[0].substring(0, 2));             // Account for weird colon character
    if (hour == 0) hour = int(timeinfo[0].substring(0, 1));  // ""
    String ampm = timeinfo[1].substring(2, 4).toLowerCase();
    println("ampm = " + ampm);
    if (ampm.equals("pm")) hour += 12;
    if (hour == 24) hour = 0;
    minute = int(timeinfo[1].substring(0, 2));
  }

  // If we got an error with time or date, stop the function and display warnings
  if (displayDateWarning || displayTimeWarning) {
    return;
  }

  // Create the new event
  Event newEvent = new Event(name, new GregorianCalendar(year, month, day, hour, minute), location);
  println("new event time: " + newEvent.getTimeString() + ", old event time: " + nextEvent.getTimeString());

  // If the event is before our previous nextEvent...
  if (newEvent.getTime().compareTo(nextEvent.getTime()) < 0) {
    // Put our previous nextEvent back in the calendar and set the nextEvent to be our new event
    CLIENT_CAL.add(nextEvent);
    nextEvent = newEvent;

    // Otherwise, add this new event to the calendar
  } else {
    CLIENT_CAL.add(newEvent);
  }

  // Sort the calendar so that it is in chronological order
  CLIENT_CAL.sort( new Comparator<Event>() {
    public int compare(Event e1, Event e2) {
      return e1.getTime().compareTo(e2.getTime());
    }
  }
  );

  // Clear all text boxes and display a feedback message
  for (TextBox box : INPUT_BOXES) box.clearTyping();
  displayAddedMessage = true;
}

// Returns true if the event is today
boolean eventIsToday(Event e) {
  GregorianCalendar date = e.getTime();
  // The event is today if the year, month and day match today's year, month, and day
  return date.get(Calendar.YEAR) == NOW.get(Calendar.YEAR) 
    && date.get(Calendar.MONTH) == NOW.get(Calendar.MONTH)
    && date.get(Calendar.DAY_OF_MONTH) == NOW.get(Calendar.DAY_OF_MONTH);
}


// ---------------- DISPLAY METHODS FOR EACH SCREEN ----------------

// Displays the add event screen
void displayAddEvent() {
  background(250);

  // Draw exit icon
  image(EXIT_ICON, width - 40, 40, 55, 55);

  // Make screen title
  textFont(REGULAR);
  textSize(50);
  fill(0);
  text("Create New Event", 250, 58);

  // Draw the text boxes
  for (TextBox box : INPUT_BOXES) { 
    box.draw();
  }
  ADD_EVENT_BUTTON.draw();

  textFont(REGULAR);
  // Write messages if necessary
  if (displayAddedMessage) {
    textSize(30);
    fill(0);
    text("Event added to calendar!", width/2, 550);
    displayAddedMessage = false;
  }
  if (displayTimeWarning) {
    textSize(20);
    fill(75);
    text("Please use the format \"hh:mm[am or pm]\"", TIME_BOX.getX() + 210, TIME_BOX.getY() + 110);
    displayTimeWarning = false;
  }
  if (displayDateWarning) {
    textSize(20);
    fill(75);
    text("Please use the format \"mm/dd/yyyy\"", DATE_BOX.getX() + 130, DATE_BOX.getY() + 110);
    displayDateWarning = false;
  }
}

// Display the time screen
void displayCurrentDateTime() {
  background(100, 0, 0);
  stroke(0);
  fill(200);

  // Display the time
  textFont(BOLD);
  textSize(145);
  text(displayTime(hour(), minute()), width/3 + 75, height/3);

  // Display the day of the week
  textFont(REGULAR);
  textSize(90);
  text(displayDayOfWeek(), width/3 + 75, height/2 + 35);

  // Display the date
  textSize(50);
  text(displayMonth() + " " + day() + ", " + year(), width/3 + 75, 2 * height / 3 + 35);
}

// Display the next event screen
void displayEventInformation() {
  background(245);

  if (eventIsToday(nextEvent)) {
    // Display explanatory text with time til event in bold, positioned
    //  according to length of display string
    int[] displayPos = getHeaderPosition(nextEvent.timeUntil());
    fill(0);
    textFont(REGULAR);
    textSize(50);
    text("Your next event is in", displayPos[0], 110);

    textFont(BOLD);
    textSize(50);
    text(nextEvent.displayTimeUntil(), displayPos[1], 110);

    // Display event name, location, and time
    fill(150, 0, 0);
    textFont(BOLD);
    textSize(getEventNameSize(nextEvent.name));
    text(nextEvent.name, width/3 + 60, height/2 - 25);

    textFont(REGULAR);
    textSize(45);
    fill(0);
    text("in " + nextEvent.location + " at " + nextEvent.getTimeString(), width/3 + 60, 2 * height/3 - 10);

    // If the next event is not today, change display accordingly
  } else { 
    fill(0);
    textFont(REGULAR);
    textSize(60);
    text("No more events today!\nCheck back tomorrow.", width/3 + 60, height/2 - 80);
  }

  // Display info about the following event in a rectangle
  fill(200, 0, 0, 60);
  stroke(200, 0, 0, 60);
  rect(width/2, height - 100, width - 100, 80, 5);

  fill(255);
  textFont(ITALIC);

  // If the next event is not today, display info about that; otherwise, 
  //     display info about the event after that
  Event followingEvent = eventIsToday(nextEvent) ? CLIENT_CAL.peek() : nextEvent;   

  if (followingEvent != null) {
    // If the following event is not today, it must be tomorrow (there are no empty 
    //    days in the LCC calendar)
    String intro = eventIsToday(followingEvent) ? "Next Event: " : "Tomorrow: ";
    String feName = followingEvent.getName(); 
    String displayString = intro + feName + " at " + followingEvent.getTimeString();
    textSize(getFollowingEventSize(displayString));
    text(displayString, width/2, height - 88);

    // Otherwise, display a message accordingly
  } else {
    textSize(40);
    text("No further events today!", width/2, height - 88);
  }
}

// ---------------- DISPLAY HELPERS ----------------


// Gets day of week based on the corresponding integer
String displayDayOfWeek() {
  switch (NOW.get(Calendar.DAY_OF_WEEK)) {
  case 2: 
    return "Monday";
  case 3: 
    return "Tuesday";
  case 4: 
    return "Wednesday";
  case 5: 
    return "Thursday";
  case 6: 
    return "Friday";
  case 0: 
    return "Saturday";
  case 1: 
    return "Sunday";
  default: 
    return "";
  }
}

// Gets month based on corresponding integer
String displayMonth() {
  switch (month()) {
  case 1: 
    return "January"; 
  case 2: 
    return "February";
  case 3: 
    return "March";
  case 4: 
    return "April";
  case 5: 
    return "May";
  case 6: 
    return "June";
  case 7: 
    return "July";
  case 8: 
    return "August";
  case 9: 
    return "September";
  case 10: 
    return "October";
  case 11: 
    return "November";
  case 12: 
    return "December";
  default: 
    return "";
  }
}

// Returns the current hour formatted in 12 hour time
int getStdHour(int hr24) {
  int hour = hr24 % 12;
  int displayHour = hour == 0 ? 12 : hour;
  return displayHour;
}

// Displays the time in hh:mm[am,pm] format
String displayTime(int hr24, int min) {
  int hour = getStdHour(hr24);
  String ampm = hr24 < 12 ? "am" : "pm";
  String addZero = min < 10 ? "0" : "";
  return hour + ":" + addZero + min + ampm;
}

// Formats the event name depending on the length of the string
float getEventNameSize(String eventName) {
  int len = eventName.length();
  if (len <= 9) return 145;
  else return 130 - (3.5 * len);
}

// Formats the following event name depending on the length of the string
float getFollowingEventSize(String followingEvent) {
  int len = followingEvent.length();
  if (len <= 40) return 40;
  else return 40 - (len * 0.1);
}

// Positions the header (time til next event) depending on length of the string
int[] getHeaderPosition(int[] timeUntil) {
  int hrs = timeUntil[0];
  int min = timeUntil[1];

  // If in format x hours xx minutes
  if (hrs > 0 && min > 0) {
    return new int[] { 285, 763 };

    // If in format xx minutes
  } else if (min > 0) {
    return new int[] { 320, 750 };

    // If in format x hours
  } else {
    return new int[] { 375, 750 };
  }
}

// Make the desired box in focus
void setFocusBox(TextBox focusBox) {
  inFocusBuffer = focusBox;
  for (TextBox box : INPUT_BOXES) {
    if (box.equals(focusBox)) box.makeInFocus();
    else box.makeOutOfFocus();
  }
}


// ------------- TEXT TO SPEECH (only for android stuff) -------------

void setUpTTS() {
  /*
  ACTIVITY = this.getActivity();
  CONTEXT = ACTIVITY.getApplicationContext();
  try {
    FD = CONTEXT.getAssets().openFd(SHARON_VARIETY);
    SHARON_SND.setDataSource(FD.getFileDescriptor(), FD.getStartOffset(), FD.getLength());
    FD.close();
    SHARON_SND.prepare();
    
    FD = CONTEXT.getAssets().openFd(NO_EVENT);
    NO_EVENT_SND.setDataSource(FD.getFileDescriptor(), FD.getStartOffset(), FD.getLength());
    FD.close();
    NO_EVENT_SND.prepare();
    
    FD = CONTEXT.getAssets().openFd(AROUND_3);
    THREE_SND.setDataSource(FD.getFileDescriptor(), FD.getStartOffset(), FD.getLength());
    FD.close();
    THREE_SND.prepare();
    
    FD = CONTEXT.getAssets().openFd(AROUND_315);
    THREE_15_SND.setDataSource(FD.getFileDescriptor(), FD.getStartOffset(), FD.getLength());
    FD.close();
    THREE_15_SND.prepare();
    
    FD = CONTEXT.getAssets().openFd(AROUND_330);
    THREE_30_SND.setDataSource(FD.getFileDescriptor(), FD.getStartOffset(), FD.getLength());
    FD.close();
    THREE_30_SND.prepare();
    
    FD = CONTEXT.getAssets().openFd(AROUND_345);
    THREE_45_SND.setDataSource(FD.getFileDescriptor(), FD.getStartOffset(), FD.getLength());
    FD.close();
    THREE_45_SND.prepare();
    
    FD = CONTEXT.getAssets().openFd(PRECISELY_349);
    THREE_49_SND.setDataSource(FD.getFileDescriptor(), FD.getStartOffset(), FD.getLength());
    FD.close();
    THREE_49_SND.prepare();
    
    FD = CONTEXT.getAssets().openFd(AROUND_4);
    FOUR_SND.setDataSource(FD.getFileDescriptor(), FD.getStartOffset(), FD.getLength());
    FD.close();
    FOUR_SND.prepare();
    
    FD = CONTEXT.getAssets().openFd(AROUND_415);
    FOUR_15_SND.setDataSource(FD.getFileDescriptor(), FD.getStartOffset(), FD.getLength());
    FD.close();
    FOUR_15_SND.prepare();
  }
  catch (IllegalArgumentException e) {
    e.printStackTrace();
  }
  catch (IllegalStateException e) {
    e.printStackTrace();
  } 
  catch (IOException e) {
    e.printStackTrace();
  }  
  */
}

void readDisplay() {
  /*
  // Get the correct sound ready to play
  MediaPlayer snd;
  if (displayEventBuffer) {
    if (minute() < 45 && hour() <= 15) snd = SHARON_SND;
    else snd = NO_EVENT_SND;
  } else {
    if (hour() == 15) {
      if (minute() < 9) snd = THREE_SND;
      else if (minute() < 23) snd = THREE_15_SND;
      else if (minute() < 38) snd = THREE_30_SND;
      else if (minute() == 49) snd = THREE_49_SND;
      else if (minute() < 53) snd = THREE_45_SND;
      else snd = FOUR_SND;
    } else {
      if (minute() < 9) snd = FOUR_SND;
      else snd = FOUR_15_SND;
    }
  }  
  
  // Play the sound
  if (!snd.isPlaying()) {
    try {
      soundLoaded = true;
      snd.start();
    }
    catch (IllegalArgumentException e) {
      e.printStackTrace();
    }
    catch (IllegalStateException e) {
      e.printStackTrace();
    }
  } else if (snd.isPlaying() && soundLoaded) {
    resumePosition = snd.getCurrentPosition();
    try {
      snd.pause();
    }
    catch (IllegalArgumentException e) {
      e.printStackTrace();
    }
    catch (IllegalStateException e) {
      e.printStackTrace();
    }
  } else if (!snd.isPlaying()) {
    snd.seekTo(resumePosition);
    snd.start();
  }
  */
}
